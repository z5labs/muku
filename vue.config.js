process.env.VUE_APP_VERSION = process.env.VERSION || "0.0.0";

module.exports = {
  publicPath: process.env.DEPLOY_URL || "/",
  chainWebpack: (config) => {
    if (process.env.NODE_ENV === "production")
      config.devtool("hidden-source-map");
  },
};
