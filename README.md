# muku

muku is an App Store designed for developers in mind.

## Getting Started

In order to get started, please go and download [muku](https://z5labs.gitlab.io/muku).
Once, you have downloaded `muku` go ahead and click on it. This should
then open a browser (or a new tab) to the 'Applications' page. These are all the
possible applications/tools that `muku` can install for you.

## Development

To begin, the overall architecture is:
```mermaid
graph LR
  app((Web App)) ---|GraphQL over WebSocket| main((Main))

  subgraph Local Processes
  main ---|gRPC over UDS| admin((Admin))
  end
```

The above diagram represents the entire design of `muku`. The two main
components are the Web App and `muku`, the binary. The binary implements
both of the local processes, `Main` and `Admin`. This "single binary, multiple
processes" architecture is akin to both Chrome and Firefox. This is advantageous
because it allows users to install only one binary/tool, which can self-manage.

### Web App

The Web App is built using [Vue.js](https://vuejs.org). It is designed to be
an offline-first Progressive Web App. The mobile first criteria of a PWA is
purposely neglected due to the nature of `muku`. To begin working on
the Web App make sure your environment is properly prepared by installing
[Node](https://nodejs.org) and the [Vue CLI](https://cli.vuejs.org). After
you have set up your environment all you need to do is install the Web Apps'
dependencies by running `npm install` at the top-level of `muku`.

### muku

`muku`, the binary, is written in [Go](https://golang.org). Go was
chosen for its simple cross-compiliability, versatile testing, and ease of
development. The binarys' implementation can be viewed as a CLI tool with
a root command and one subcommand. The root command, `muku`, corresponds
to the `Main` process in the above diagram and the subcommand,
`muku installer`, corresponds to the `Admin` process.

Before trying to work on `muku` make sure you have installed the current
version of [Go](https://golang.org/doc/install). After installing Go, go ahead
and change directories into `cmd/muku`, then run `go generate ./...`.
The generate command will auto-generate some files which aren't checked into
git.

## Contributing

Follow Conventional Commit style. If unsure check past commit messages for an
example.

TODO: Add Contributing guidelines
