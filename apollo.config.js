module.exports = {
  client: {
    service: {
      name: "muku",
      localSchemaFile: "./graphql/api.gql",
    },
    includes: ["src/**/*.{js,jsx,ts,tsx,vue,gql}"],
  },
};
