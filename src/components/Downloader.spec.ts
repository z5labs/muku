import VueComposition from "@vue/composition-api";
import { createLocalVue, mount } from "@vue/test-utils";
import Downloader from "@/components/Downloader.vue";
import MatListItem from "@/material/MatListItem.vue";

interface SysInfo {
  arch: string;
  os: string;
}

const localVue = createLocalVue();
localVue.use(VueComposition);

describe("Downloader", () => {
  it("should download", async () => {
    const wrapper = mount(Downloader, {
      localVue,
      propsData: {
        arch: "",
        os: "",
      },
    });

    wrapper.find('[data-text="select-arch"]').trigger("focus");
    await wrapper.vm.$nextTick();
    wrapper
      .find('[data-text="archs"]')
      .findAll(MatListItem)
      .at(1)
      .trigger("click");
    await wrapper.vm.$nextTick();

    wrapper.find('[data-text="select-os"]').trigger("focus");
    await wrapper.vm.$nextTick();
    wrapper
      .find('[data-text="oses"]')
      .findAll(MatListItem)
      .at(2)
      .trigger("click");
    await wrapper.vm.$nextTick();

    expect(((wrapper.vm as unknown) as { sys: SysInfo }).sys.arch).toBe("64");
    expect(((wrapper.vm as unknown) as { sys: SysInfo }).sys.os).toBe("linux");
  });
});
