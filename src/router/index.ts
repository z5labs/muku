import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Welcome from "@/views/Welcome.vue";
import Apps from "@/views/Apps.vue";

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    beforeEnter: (to, from, next) => {
      if ("port" in to.query) next();
      else next("/welcome");
    },
    props: (route) => ({ port: parseInt(route.query.port as string) }),
    component: () => import("@/views/Home.vue"),
    children: [
      {
        path: "",
        name: "Apps",
        component: Apps,
      },
      {
        path: "app/:id",
        component: () => import("@/views/App.vue"),
        children: [
          {
            path: "",
            name: "Info",
            props: (route) => ({
              info: JSON.parse(route.query.info as string),
            }),
            component: () => import("@/views/Info.vue"),
          },
          {
            path: "install",
            name: "Install",
            props: (route) => ({ id: route.params.id }),
            component: () => import("@/views/Install.vue"),
          },
        ],
      },
      {
        path: "dev",
        component: () => import("@/views/Dev.vue"),
        children: [
          {
            path: "",
            redirect: "logs",
          },
          {
            path: "logs",
            name: "Logs",
            component: () => import("@/views/Logs.vue"),
          },
        ],
      },
    ],
  },
  {
    path: "/download",
    name: "Download",
    component: () => import("@/views/Download.vue"),
  },
  {
    path: "/welcome",
    name: "Welcome",
    component: Welcome,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
