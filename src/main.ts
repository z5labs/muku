import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import VueComposition from "@vue/composition-api";
import router from "./router";

Vue.use(VueComposition);

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
