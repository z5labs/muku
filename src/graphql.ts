import { inject, provide } from "@vue/composition-api";
import ApolloClient from "apollo-boost";

export const ApolloSymbol = Symbol();

export function provideApollo(client: ApolloClient<unknown>): void {
  provide(ApolloSymbol, client);
}

export function useApollo(): ApolloClient<unknown> {
  return inject(ApolloSymbol) as ApolloClient<unknown>;
}
