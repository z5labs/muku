package browser

import "os/exec"

func which(name string) string {
	p, err := exec.LookPath(name)
	if err != nil {
		return "" // TODO: probably shouldn't skip... probs log
	}
	return p
}
