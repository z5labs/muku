package browser

import (
	"os"
	"path/filepath"
)

type WindowsDefault struct{}

func (b *WindowsDefault) Open(url string, typ OpenAs) error {
	return nil
}

func registerBrowsers(order *browsers) {
	order.Register(&browserDescr{
		name: "windows-default",
		Browser: &browserCmd{
			cmd:  "rundll32",
			args: []string{"url.dll,FileProtocolHandler"},
		},
	})

	// Find IE
	progFiles := os.Getenv("PROGRAMFILES")
	if progFiles == "" {
		progFiles = "C:\\Program Files"
	}
	iexplore := filepath.Join(progFiles, "Internet Explorer\\IEXPLORE.EXE")

	// Check for other common browsers
	for _, name := range []string{"firefox", "firebird", "seamonkey", "mozilla", "netscape", "opera", iexplore} {
		p := which(name)
		if p == "" {
			continue
		}

		// TODO: Check for absoluteness of p
		order.Register(&browserDescr{
			name:    name,
			Browser: &browserCmd{cmd: name},
		})
	}

	// Always prioritize Chrome for now
	if p := which("chrome"); p != "" {
		order.Register(&browserDescr{
			name: "chrome",
			Browser: &browserCmd{
				cmd:  "start",
				args: []string{"chrome"},
			},
		})
	}
	if p := which("google-chrome"); p != "" {
		order.Register(&browserDescr{
			name: "google-chrome",
			Browser: &browserCmd{
				cmd:  "start",
				args: []string{"google-chrome"},
			},
		})
	}
}
