package browser

import (
	"bytes"
	"strings"
)

type MacOSXOSAScript struct {
	Name string
}

func (b *MacOSXOSAScript) Open(url string, typ OpenAs) error {
	var buf bytes.Buffer

	switch b.Name {
	case "default":
		buf.WriteString("open location ")
		buf.WriteRune('"')
		buf.WriteString(strings.Replace(url, "\"", "%22", -1))
		buf.WriteRune('"')
		break
	default:
		buf.WriteString("tell application ")
		buf.WriteRune('"')
		buf.WriteString(b.Name)
		buf.WriteRune('"')
		buf.WriteString("\n\tactivate\n")
		buf.WriteString("\topen location ")
		buf.WriteRune('"')
		buf.WriteString(strings.Replace(url, "\"", "%22", -1))
		buf.WriteRune('"')
		buf.WriteString("\nend")
		break
	}

	cmd := command("osascript", "w")
	cmd.Stdin = &buf
	return cmd.Run()
}

func registerBrowsers(order *browsers) {
	order.Register(&browserDescr{
		name:    "MacOSX",
		Browser: &MacOSXOSAScript{"default"},
	})
	order.Register(&browserDescr{
		name:    "chrome",
		Browser: &MacOSXOSAScript{"chrome"},
	})
	order.Register(&browserDescr{
		name:    "firefox",
		Browser: &MacOSXOSAScript{"firefox"},
	})
	order.Register(&browserDescr{
		name:    "safari",
		Browser: &MacOSXOSAScript{"safari"},
	})
}
