// Package browser
package browser

import (
	"container/heap"
	"io"
	"os"
	"os/exec"
	"strings"
)

// command alias' exec.Command for browser testability
var command = exec.Command

// TODO: clear order after browser has been opened
var order browsers

func init() {
	registerBrowsers(&order)
	heap.Init(&order)

	// Allow BROWSER env var to override priorities
	bEnv, ok := os.LookupEnv("BROWSER")
	if !ok {
		return
	}

	choices := strings.Split(bEnv, string(os.PathSeparator))

	// Treat choices in same way as if passed into get() but do register
	// and prepend to _tryorder
	//      for cmdline in userchoices:
	//          if cmdline != '':
	//              cmd = _synthesize(cmdline, preferred=True)
	//              if cmd[1] is None:
	//                  register(cmdline, None, GenericBrowser(cmdline), preferred=True)
	cLen := len(choices)
	for i, c := range choices {
		if c == "" {
			continue
		}

		// TODO: Don't re-add browsers from env var just update their priority
		for j, b := range order {
			if b.name == c {
				b.priority += uint8(cLen - i)
				heap.Fix(&order, j)
			}
		}
	}
}

// OpenAs represents how the URL should be opened
type OpenAs uint8

const (
	// Open in new tab
	TAB OpenAs = 1 << iota

	// Open in new window
	NEW_WINDOW
)

// TODO: maybe not export this?
// Browser represents a local browser
type Browser interface {
	// Open opens the browser to the given url
	Open(url string, typ OpenAs) error
}

func open(url string, typ OpenAs) error {
	if len(order) == 0 {
		return nil // TODO: Handle
	}

	for _, browser := range order {
		return browser.Open(url, typ)
	}
	return nil
}

// Open opens a users' browser to the given URL
func Open(url string) error {
	return open(url, NEW_WINDOW)
}

// OpenTab opens a tab in a current browser
func OpenTab(url string) error {
	return open(url, TAB)
}

// OpenWindow opens a new browser window.
func OpenWindow(url string) error {
	return open(url, NEW_WINDOW)
}

// browserCmd implements the Browser interface for *exec.Command.
type browserCmd struct {
	cmd      string
	args     []string
	out, err io.Writer
}

func (c *browserCmd) Open(url string, typ OpenAs) error {
	if c.out == nil {
		c.out = os.Stdout
	}
	if c.err == nil {
		c.err = os.Stderr
	}

	cmd := command(c.cmd, append(c.args, url)...)
	cmd.Stdout = c.out
	cmd.Stderr = c.err
	return cmd.Run()
}
