package browser

type browserDescr struct {
	Browser
	name     string
	klass    string
	priority uint8 // make it a priority queue
	index    int
}

// browsers is a list of available browsers.
type browsers []*browserDescr

// Register adds a browser to the list
func (o *browsers) Register(descr *browserDescr) {
	o.Push(descr)
}

func (o *browsers) Len() int {
	return len(*o)
}

func (o *browsers) Less(i, j int) bool {
	return (*o)[i].priority > (*o)[j].priority
}

func (o *browsers) Swap(i, j int) {
	(*o)[i], (*o)[j] = (*o)[j], (*o)[i]
	(*o)[i].index = j
	(*o)[j].index = i
}

func (o *browsers) Push(x interface{}) {
	n := len(*o)
	b := x.(*browserDescr)
	b.index = n
	*o = append(*o, b)
}

func (o *browsers) Pop() interface{} {
	old := *o
	n := len(old)
	b := old[n-1]
	old[n-1] = nil // avoid memory leak
	b.index = -1   // for safety
	*o = old[0 : n-1]
	return b
}
