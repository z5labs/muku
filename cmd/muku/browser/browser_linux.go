package browser

import (
	"bytes"
	"os"
	"strings"
)

type Elinks struct{}

func (b *Elinks) Open(url string, typ OpenAs) error {
	return nil
}

func registerBrowsers(order *browsers) {
	_, ok := os.LookupEnv("DISPLAY")
	if !ok {
		return
	}

	var b bytes.Buffer
	cmd := command("xdg-settings", "get", "default-web-browser")
	cmd.Stdout = &b

	err := cmd.Run()
	if err != nil {
		return // TODO: Handle
	}

	registerXBrowsers(order, strings.TrimSpace(b.String()))
}

var xBrowsers = []struct {
	descr  *browserDescr
	exists func(string) bool
}{
	{ // use xdg-open if its there
		descr: &browserDescr{
			name:    "xdg-open",
			Browser: &browserCmd{cmd: "xdg-open"},
		},
	},
	{ // The default GNOME3 browser
		descr: &browserDescr{
			name: "gvfs-open",
			Browser: &browserCmd{
				cmd: "gvfs-open",
			},
		},
		exists: func(_ string) bool {
			_, ok := os.LookupEnv("GNOME_DESKTOP_SESSION_ID")
			return ok && which("gvfs-open") != ""
		},
	},
	{ // The default GNOME browser
		descr: &browserDescr{
			name: "gnome-open",
			Browser: &browserCmd{
				cmd: "gnome-open",
			},
		},
		exists: func(_ string) bool {
			_, ok := os.LookupEnv("GNOME_DESKTOP_SESSION_ID")
			return ok && which("gnome-open") != ""
		},
	},
	// The Mozilla browsers
	{
		descr: &browserDescr{
			name: "firefox",
			Browser: &browserCmd{
				cmd: "firefox",
			},
		},
	},
	// Google Chrome/Chromium browsers
	{
		descr: &browserDescr{
			name: "google-chrome",
			Browser: &browserCmd{
				cmd: "google-chrome",
			},
		},
	},
	{
		descr: &browserDescr{
			name: "chrome",
			Browser: &browserCmd{
				cmd: "chrome",
			},
		},
	},
	{
		descr: &browserDescr{
			name: "chromium",
			Browser: &browserCmd{
				cmd: "chromium",
			},
		},
	},
	{
		descr: &browserDescr{
			name: "chromium-browser",
			Browser: &browserCmd{
				cmd: "chromium-browser",
			},
		},
	},
	{ // Opera, quite popular
		descr: &browserDescr{
			name: "opera",
			Browser: &browserCmd{
				cmd: "opera",
			},
		},
	},
}

func exists(name string) bool {
	return which(name) != ""
}

func registerXBrowsers(order *browsers, preferred string) {
	var priority uint8 = 0

	for _, xb := range xBrowsers {
		if xb.exists == nil {
			xb.exists = exists
		}
		if !xb.exists(xb.descr.name) {
			continue
		}

		if preferred == xb.descr.name {
			priority = 1
		}

		xb.descr.priority = priority

		order.Register(xb.descr)

		priority = 0
	}
}
