package browser

import (
	"flag"
	"testing"
)

var openBrowser = flag.Bool("open-browser", false, "Run browser test")

func TestOpen(t *testing.T) {
	if !*openBrowser {
		t.Skip("skipping browser test")
		return
	}

	if err := Open("https://google.com"); err != nil {
		t.Fatalf("%v", err)
		return
	}
}
