package super

import (
	"os"
	"testing"
)

func TestWithPkexec(t *testing.T) {
	if !*prompt {
		t.Skip("skipping linux prompt")
		return
	}

	user := NewUser("pkexec")

	cmd := user.Command(os.Args[0], "-test.run=TestHelperProcess", "--", "GO_WANT_HELPER_PROCESS=1")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		t.Error(err)
	}
}
