// Package super is for running process as a system super-user/admin.
package super

import (
	"os/exec"
)

// User represents a systems user with elevated priviledges
// e.g. root in Unix
//
type User struct {
	sudoer     string
	sudoerArgs []string
}

// NewUser returns a new user.
func NewUser(sudoer string, sudoerArgs ...string) *User {
	return &User{
		sudoer,
		sudoerArgs,
	}
}

// Command returns the super user version of the given command
func (u *User) Command(cmd string, args ...string) *exec.Cmd {
	return exec.Command(u.sudoer, append(u.sudoerArgs, append([]string{cmd}, args...)...)...)
}
