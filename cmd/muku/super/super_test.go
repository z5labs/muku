package super

import (
	"bytes"
	"flag"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"testing"
)

var local = flag.Bool("local", false, "run tests in an environment that will work")
var prompt = flag.Bool("prompt", false, "use system gui to prompt for password")

func TestCommand(t *testing.T) {
	if !*local {
		t.Skip("skipping super tests")
		return
	}

	var user *User
	switch runtime.GOOS {
	case "windows":
		// TODO: possibly use runas command
		break
	default:
		user = NewUser("sudo", "GO_WANT_HELPER_PROCESS=1")
		break
	}

	cmd := user.Command(os.Args[0], "-test.run=TestHelperProcess", "--")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		t.Error(err)
	}
}

func TestHelperProcess(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") != "1" && !isSubProc(os.Args) {
		t.Log("skipping")
		return
	}

	// I should be root so I can run commands as such
	var b bytes.Buffer
	cmd := exec.Command("ls", "/root")
	cmd.Stdout = &b
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	if err != nil {
		t.Error(err)
	}

	ls := strings.TrimSpace(b.String())
	if ls != "" {
		t.Log(ls)
		t.Fail()
	}
}

func isSubProc(args []string) bool {
	for _, arg := range args {
		if arg == "--" {
			return true
		}
	}
	return false
}
