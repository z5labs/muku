package super

import (
	"os"
	"testing"
)

func TestWithOSAScript(t *testing.T) {
	if !*prompt {
		t.Skip("skipping darwin prompt")
		return
	}

	user := NewUser("osascript", "-e", "'do shell script \"")

	cmd := user.Command(os.Args[0], "-test.run=TestHelperProcess", "--", "GO_WANT_HELPER_PROCESS=1", "\" ' with administrator priviledges")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		t.Error(err)
	}
}
