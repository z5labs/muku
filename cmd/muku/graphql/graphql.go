package graphql

import (
	"context"
	"fmt"

	"muku/app"
	pb "muku/cmd/proto"

	"github.com/graphql-go/graphql"
	"github.com/zaba505/gws"
	"go.uber.org/zap/zaptest/observer"
)

// Schema returns the GraphQL schema this package implements.
func Schema() (graphql.Schema, error) {
	return graphql.NewSchema(graphql.SchemaConfig{
		Query:        QueryType,
		Subscription: SubscriptionType,
	})
}

// Option
type ContextOption interface {
	with(parent context.Context) context.Context
}

type optionFunc func(context.Context) context.Context

func (f optionFunc) with(parent context.Context) context.Context { return f(parent) }

// NewContext derives a context.Context for supplying to the
// GraphQL runtime. i.e. the resolvers
//
func NewContext(parent context.Context, opts ...ContextOption) (ctx context.Context) {
	ctx = parent
	for _, opt := range opts {
		ctx = opt.with(ctx)
	}
	return
}

// CtxErr corresponds to an ill-prepared request context.
type CtxErr struct {
	// Expected is what was expected to be in the context.
	Expected string
}

func (e *CtxErr) Error() string {
	return fmt.Sprintf("missing value in request context: %s", e.Expected)
}

type ctxKey string

// internal context keys
const (
	clientKey   ctxKey = "app-client"
	logCacheKey ctxKey = "log-cache"
	registryKey ctxKey = "app-registry"
	streamKey   ctxKey = "stream"
)

// WithStream
func WithStream(stream *gws.Stream) ContextOption {
	return optionFunc(func(ctx context.Context) context.Context {
		return context.WithValue(ctx, streamKey, stream)
	})
}

func getStream(ctx context.Context) *gws.Stream {
	return ctx.Value(streamKey).(*gws.Stream)
}

// WithClient adds a pb.AppClient to the context.
func WithClient(client pb.AppClient) ContextOption {
	return optionFunc(func(ctx context.Context) context.Context {
		return context.WithValue(ctx, clientKey, client)
	})
}

func getClient(ctx context.Context) (pb.AppClient, bool) {
	client, ok := ctx.Value(clientKey).(pb.AppClient)
	return client, ok
}

// WithLogCache adds a observer.ObservedLogs to the context.
func WithLogCache(cache *observer.ObservedLogs) ContextOption {
	return optionFunc(func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCacheKey, cache)
	})
}

func getLogCache(ctx context.Context) (*observer.ObservedLogs, bool) {
	cache, ok := ctx.Value(logCacheKey).(*observer.ObservedLogs)
	return cache, ok
}

// WithAppRegistry adds a App Registry to the context.
func WithAppRegistry(reg *app.Registry) ContextOption {
	return optionFunc(func(ctx context.Context) context.Context {
		return context.WithValue(ctx, registryKey, reg)
	})
}

func getRegistry(ctx context.Context) (*app.Registry, bool) {
	reg, ok := ctx.Value(registryKey).(*app.Registry)
	return reg, ok
}
