//go:generate go run github.com/gqlc/gqlc --go_out . --go_opt=package=graphql ../../../graphql/api.gql

package graphql

import (
	"context"
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"

	"muku/app"
	pb "muku/cmd/proto"

	"github.com/graphql-go/graphql"
	"github.com/zaba505/gws"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"go.uber.org/zap/zaptest/observer"
)

type sysInfo struct {
	os       string
	arch     string
	rootPath string
}

func getSystemInfo(p graphql.ResolveParams) (interface{}, error) {
	var s string
	switch runtime.GOOS {
	case "windows":
		s = os.Getenv("SYSTEMROOT")
		if s == "" {
			s = "C:\\\\"
		}
	case "darwin", "linux":
		s = "/"
	}

	return &sysInfo{
		os:       runtime.GOOS,
		arch:     runtime.GOARCH,
		rootPath: s,
	}, nil
}

func getSystemOs(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(*sysInfo).os, nil
}

func getSystemArch(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(*sysInfo).arch, nil
}

func getRootPath(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(*sysInfo).rootPath, nil
}

func walkPath(p graphql.ResolveParams) (interface{}, error) {
	var paths []string

	depth := p.Args["depth"].(int)
	if depth == 0 {
		return paths, nil
	}

	root := p.Args["root"].(string)
	vol := filepath.VolumeName(root)

	if depth < 0 {
		root = filepath.Dir(root)
		depth = 1
	}

	return paths, filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			return err
		}
		if os.IsPermission(err) || filepath.Base(path)[0] == '.' {
			return filepath.SkipDir
		}

		paths = append(paths, path)
		if path == root {
			return nil
		}

		tPath := strings.TrimPrefix(path, root)

		dirs := strings.Split(tPath, string(filepath.Separator))
		if dirs[0] == vol {
			dirs = dirs[1:]
		}

		if len(dirs) == depth {
			return filepath.SkipDir
		}

		return err
	})
}

func getApps(p graphql.ResolveParams) (interface{}, error) {
	reg, ok := getRegistry(p.Context)
	if !ok {
		return nil, &CtxErr{Expected: "app registry"}
	}

	return reg.AllInfo(), nil
}

func getAppID(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(app.Info).Id, nil
}

func getAppDescription(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(app.Info).Description, nil
}

func getAppVersion(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(app.Info).Version, nil
}

func getAppStatus(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(app.Info).Installed, nil
}

func getAppPath(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(app.Info).Path, nil
}

func getAppThumbnail(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(app.Info).Thumbnail, nil
}

type logEntry struct {
	logger string
	level  string
	time   string
	msg    string
	meta   map[string]string
}

func getLogs(p graphql.ResolveParams) (interface{}, error) {
	cache, ok := getLogCache(p.Context)
	if !ok {
		return nil, &CtxErr{Expected: "log cache"}
	}

	entries := cache.All()

	// TODO: Implement after and before filters

	lvlStr, ok := p.Args["level"].(string)
	if ok && lvlStr != "" {
		var lvl zapcore.Level
		err := lvl.UnmarshalText([]byte(lvlStr))
		if err != nil {
			return nil, err
		}

		entries = filterByLevel(entries, lvl)
	}

	fieldName, ok := p.Args["key"].(string)
	if ok && fieldName != "" {
		entries = filterByField(entries, fieldName)
	}

	n, ok := p.Args["first"].(int)
	if ok && n > 0 {
		entries = entries[:n]
	}

	return entries, nil
}

func filterByField(entries []observer.LoggedEntry, fieldName string) (es []observer.LoggedEntry) {
	for _, entry := range entries {
		m := entry.ContextMap()
		if _, ok := m[fieldName]; ok {
			es = append(es, entry)
		}
	}

	return
}

func filterByLevel(entries []observer.LoggedEntry, level zapcore.Level) (es []observer.LoggedEntry) {
	for _, entry := range entries {
		if entry.Level == level {
			es = append(es, entry)
		}
	}

	return
}

func getLogLogger(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(observer.LoggedEntry).LoggerName, nil
}

func getLogLevel(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(observer.LoggedEntry).Level.String(), nil
}

func getLogTimestamp(p graphql.ResolveParams) (interface{}, error) {
	ltime := p.Source.(observer.LoggedEntry).Time
	timeFormat := p.Args["format"].(string)

	return ltime.Format(timeFormat), nil
}

func getLogMsg(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(observer.LoggedEntry).Message, nil
}

type kvPair struct {
	key, val string
}

func getLogMeta(p graphql.ResolveParams) (interface{}, error) {
	meta := p.Source.(observer.LoggedEntry).ContextMap()
	pairs := make([]kvPair, 0, len(meta))
	for k, v := range meta {
		pairs = append(pairs, kvPair{key: k, val: toString(v)})
	}

	return pairs, nil
}

func toString(v interface{}) (s string) {
	switch x := v.(type) {
	case string:
		s = x
	case int:
		s = strconv.Itoa(x)
	case int64:
		s = strconv.FormatInt(x, 10)
	}
	return
}

func getKey(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(kvPair).key, nil
}

func getValue(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(kvPair).val, nil
}

type failed string
type inProgress string

func install(p graphql.ResolveParams) (interface{}, error) {
	ctx := p.Context
	s := getStream(ctx)

	client, ok := getClient(ctx)
	if !ok {
		return nil, &CtxErr{Expected: "app client"}
	}

	req := p.Args["req"].(map[string]interface{})
	id := req["id"].(string)
	v := req["version"].(string)
	path := req["path"].(string)

	stream, err := client.Install(ctx, &pb.AppInfo{Id: id, Version: v, Path: path})
	if err != nil {
		zap.L().Error("unexpected error from client install rpc call", zap.Error(err))
		return nil, err
	}

	go func() {
		defer s.Close()

		for {
			stat, err := stream.Recv()
			if err != nil {
				if err != io.EOF {
					zap.L().Error("unexpected error when reading from grpc stream", zap.Error(err))
				}
				return
			}

			resp := &gws.Response{}
			switch v := stat.Status.(type) {
			case *pb.InstallStatus_Failed:
				resp.Errors = append(resp.Errors, json.RawMessage(v.Failed))
			case *pb.InstallStatus_Progress:
				resp.Data = json.RawMessage(v.Progress)
			case *pb.InstallStatus_Installed:
				resp.Data, err = json.Marshal(v.Installed)
				if err != nil {
					// TODO: Handle marshal error
					return
				}
			}

			ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)

			err = s.Send(ctx, resp)
			cancel()

			if err != nil {
				zap.L().Error("failed to send on stream", zap.Error(err), zap.String("data", string(resp.Data)))
				return
			}
		}
	}()

	return inProgress("started"), nil
}

func getInstallError(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(failed), nil
}

func getProgress(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(inProgress), nil
}

func getInstallPath(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(*pb.Installed).Path, nil
}

func getInstallTime(p graphql.ResolveParams) (interface{}, error) {
	return p.Source.(*pb.Installed).Time, nil
}

func getInstallStatus(p graphql.ResolveTypeParams) *graphql.Object {
	switch p.Value.(type) {
	case *pb.Installed:
		return InstalledType
	case failed:
		return FailedType
	case inProgress:
		return InProgressType
	default:
		panic("unexpected type")
	}
}
