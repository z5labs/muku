package cmd

import (
	"context"
	"io"
	"os"
	"os/exec"
	"testing"

	"muku/app"
	pb "muku/cmd/proto"

	"github.com/golang/protobuf/proto"
	"google.golang.org/grpc"
)

type testApp struct{}

func (a *testApp) Info() app.Info {
	return app.Info{Id: "test"}
}

func (a *testApp) Install(_ context.Context, path string, version app.Version) <-chan app.Status {
	statChan := make(chan app.Status)

	go func() {
		defer close(statChan)

		statChan <- app.Status{Progress: app.Downloading}
		statChan <- app.Status{Progress: app.Downloaded}
		statChan <- app.Status{Progress: app.Building}
		statChan <- app.Status{Progress: app.Installing}
		statChan <- app.Status{Installed: &app.Installed{Path: "/", Time: 1}}
	}()

	return statChan
}

func TestInstaller(t *testing.T) {
	// Start installer process
	cmd := exec.Command(os.Args[0], "-test.run=TestHelperProcess", "--", "tcs-installer", "installer", "-s", "/tmp/test.sock")
	cmd.Env = []string{"GO_WANT_HELPER_PROCESS=1"}
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Start(); err != nil {
		t.Error(err)
		return
	}
	defer cmd.Wait()
	defer cmd.Process.Kill()

	// Connect via gRPC over UDS
	conn, err := grpc.Dial("unix:///tmp/test.sock", grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		t.Error(err)
		return
	}
	defer conn.Close()

	client := pb.NewAppClient(conn)

	// Send req
	stream, err := client.Install(context.Background(), &pb.AppInfo{Id: "test", Version: "latest"})
	if err != nil {
		t.Error(err)
		return
	}

	resps := []*pb.InstallStatus{
		&pb.InstallStatus{Status: &pb.InstallStatus_Progress{Progress: string(app.Downloading)}},
		&pb.InstallStatus{Status: &pb.InstallStatus_Progress{Progress: string(app.Downloaded)}},
		&pb.InstallStatus{Status: &pb.InstallStatus_Progress{Progress: string(app.Building)}},
		&pb.InstallStatus{Status: &pb.InstallStatus_Progress{Progress: string(app.Installing)}},
		&pb.InstallStatus{
			Status: &pb.InstallStatus_Installed{
				Installed: &pb.Installed{
					Path: "/",
					Time: 1,
				},
			},
		},
	}

	// Monitor stream
	for {
		status, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			t.Fatalf("%v.ListFeatures(_) = _, %v", client, err)
			return
		}

		if !proto.Equal(status, resps[0]) {
			t.Fail()
			return
		}

		if len(resps) > 1 {
			resps = resps[1:]
			continue
		}
		resps = resps[:0]
	}

	if len(resps) > 0 {
		t.Fail()
	}
}

func TestHelperProcess(t *testing.T) {
	if os.Getenv("GO_WANT_HELPER_PROCESS") != "1" {
		return
	}

	os.Args = os.Args[3:]

	apps := app.NewRegistry()
	apps.Register(&testApp{})

	cmd := NewCLI(apps)

	err := cmd.Execute()
	if err != nil {
		t.Error(err)
		return
	}
}
