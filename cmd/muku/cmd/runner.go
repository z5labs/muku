package cmd

import (
	"context"
	"errors"
)

// ErrShutdown tells the runner to shutdown and capture the next non-nil error.
var ErrShutdown = errors.New("shutdown runner")

// runner orchestrates goroutines.
type runner struct {
	num     int32
	workers []func(context.Context) error
}

func (r *runner) Go(f func(context.Context) error) {
	r.num++
	r.workers = append(r.workers, f)
}

func (r *runner) Wait(ctx context.Context) (err error) {
	runCtx, cancel := context.WithCancel(ctx)
	defer cancel()

	errChan := make(chan error, len(r.workers))
	defer close(errChan)

	for _, worker := range r.workers {
		go func(f func(context.Context) error) {
			err := f(runCtx)
			errChan <- err
		}(worker)
	}

	for {
		if r.num == 0 {
			return
		}

		select {
		case e := <-errChan:
			r.num--
			if e == nil {
				break
			}
			cancel()

			if err == nil && e != ErrShutdown {
				err = e
			}

			break
		case <-runCtx.Done():
			break
		}
	}
}

func (r *runner) drain(errChan <-chan error) (err error) {
	for {
		if r.num == 0 {
			return
		}

		r.num--
		e := <-errChan
		if err == nil && e != nil {
			err = e
		}
	}
}
