package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"

	"muku/app"
	"muku/browser"
	pb "muku/cmd/proto"
	"muku/graphql"
	"muku/super"

	empty "github.com/golang/protobuf/ptypes/empty"
	gql "github.com/graphql-go/graphql"
	"github.com/spf13/cobra"
	"github.com/zaba505/gws"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"go.uber.org/zap/zaptest/observer"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/connectivity"
	"google.golang.org/grpc/status"
)

type logConfig struct {
	close func()
	out   zapcore.WriteSyncer
	cache *observer.ObservedLogs
}

type rootCmd struct {
	*cobra.Command

	apps        *app.Registry
	srv         *http.Server
	schema      gql.Schema
	ls          net.Listener
	subProc     *exec.Cmd
	subProcConn *grpc.ClientConn
}

func (cmd *rootCmd) getCommand() *cobra.Command { return cmd.Command }

func (c *cli) newRootCmd(apps *app.Registry) *rootCmd {
	profOuts := new(profs)

	root := &rootCmd{
		Command: &cobra.Command{
			Use:           "muku",
			Short:         "A desktop manager for everything.",
			SilenceErrors: true,
			SilenceUsage:  true,
		},
		srv:  &http.Server{},
		apps: apps,
	}

	tmpDir := os.TempDir()
	if tmpDir == "" {
		tmpDir, _ = os.Getwd()
	}
	sock := socketAddrFlag(filepath.Join(tmpDir, "muku.sock"))

	root.Flags().StringP("domain", "d", "https://z5labs.gitlab.io/muku", "This is the domain for the UI.")
	root.Flags().BoolP("browser", "b", true, "Open browser on startup.")
	root.Flags().StringP("port", "p", "0", "Specify a port for the GraphQL service.")
	root.Flags().VarP(&sock, "socket", "s", "Name a specific UDS for IPC.")
	root.Flags().String("log-output", filepath.Join(tmpDir, "muku.logs"), "Specify output for logs.")

	root.PersistentFlags().Var(&logLevelFlag{}, "log-level", "Specify the log output level.")
	root.PersistentFlags().String("log-format", "json", "Specify logs format. (Options: txt, json)")
	root.PersistentFlags().String("trace", "", "Output tracing profile to the provided directory.")
	root.PersistentFlags().String("profile-cpu", "", "Output a cpu profile to the provided directory.")
	root.PersistentFlags().String("profile-mem", "", "Output a mem profile to the provided directory.")

	root.PersistentFlags().MarkHidden("trace")
	root.PersistentFlags().MarkHidden("profile-cpu")
	root.PersistentFlags().MarkHidden("profile-mem")

	logCfg := &logConfig{}

	root.PersistentPreRunE = withPreRuns(
		handleProfErrors(profOuts),
		startTracing(profOuts),
		startPprofProfiles(profOuts),
		configureLogger(logCfg),
	)

	root.PreRunE = withPreRuns(
		handleError(root),
		initSchema(root),
		listen(root),
		configSubProc(root),
		captureSubProcLogs(root, logCfg),
		connectToSubProc(root),
	)

	root.RunE = func(c *cobra.Command, args []string) error {
		openBrowser, err := c.Flags().GetBool("browser")
		if err != nil {
			return err
		}

		domain, err := c.Flags().GetString("domain")
		if err != nil {
			return err
		}

		run(c.Context(), root, logCfg, openBrowser, domain)
		return nil
	}

	root.PersistentPostRunE = withPreRuns(
		handleProfErrors(profOuts),
		stopTracing(profOuts),
		stopPprofProfiles(profOuts),
		writeMemProfile,
		cleanupLogger(logCfg),
	)

	return root
}

func handleProfErrors(p *profs) func(error) {
	return func(_ error) {
		if p.traceOut != nil {
			wc, ok := p.traceOut.(io.WriteCloser)
			if !ok {
				return
			}
			wc.Close()
		}

		if p.cpuOut != nil {
			wc, ok := p.cpuOut.(io.WriteCloser)
			if !ok {
				return
			}
			wc.Close()
		}
	}
}

func configureLogger(l *logConfig) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		logFmt, err := cmd.Flags().GetString("log-format")
		if err != nil {
			return err
		}

		cfg := zap.NewProductionEncoderConfig()

		var enc zapcore.Encoder
		switch logFmt {
		case "json":
			enc = zapcore.NewJSONEncoder(cfg)
			break
		case "txt":
			enc = zapcore.NewConsoleEncoder(cfg)
			break
		default:
			return fmt.Errorf("unsupported log format: %s", logFmt)
		}

		l.out = zapcore.Lock(os.Stderr)
		outputFlag := cmd.Flags().Lookup("log-output")
		if outputFlag != nil {
			switch outName := outputFlag.Value.String(); outName {
			case "stderr":
				break
			case "stdout":
				fallthrough
			default:
				l.out, l.close, err = zap.Open(outName)
				if err != nil {
					return err
				}
			}
		}

		lvlFlag := cmd.Flags().Lookup("log-level")
		if lvlFlag == nil {
			return fmt.Errorf("expected log level flag")
		}
		lvl := lvlFlag.Value.(*logLevelFlag).Level

		name := cmd.Name()
		if name == "muku" {
			name = "main"
		}

		outCore := zapcore.NewCore(enc, l.out, lvl)
		cacheCore, cache := observer.New(lvl)
		l.cache = cache

		logger := zap.New(zapcore.NewTee(outCore, cacheCore))

		zap.ReplaceGlobals(logger.Named(name))
		return nil
	}
}

func cleanupLogger(l *logConfig) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		if l.close == nil {
			return nil
		}
		l.close()
		return nil
	}
}

func handleError(p *rootCmd) func(error) {
	return func(_ error) {
		if p.ls != nil {
			p.ls.Close()
		}
	}
}

func initSchema(p *rootCmd) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) (err error) {
		p.schema, err = graphql.Schema()
		return
	}
}

func listen(p *rootCmd) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		port, err := cmd.Flags().GetString("port")
		if err != nil {
			return err
		}

		p.ls, err = net.Listen("tcp", "localhost:"+port)
		return err
	}
}

func configSubProc(p *rootCmd) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		logFmt, err := cmd.Flags().GetString("log-format")
		if err != nil {
			return err
		}

		logLvlFlag := cmd.Flags().Lookup("log-level")
		if logLvlFlag == nil {
			return fmt.Errorf("expected log level flag")
		}
		logLvl := logLvlFlag.Value.String()

		sockFlag := cmd.Flags().Lookup("socket")
		if sockFlag == nil {
			return fmt.Errorf("expected socket flag")
		}
		sock := sockFlag.Value.String()

		binName, err := getCurBin(os.Args[0])
		if err != nil {
			return err
		}

		switch runtime.GOOS {
		case "darwin":
			user := super.NewUser("osascript", "-e", "'do shell script \"")
			p.subProc = user.Command(binName, "installer", "-s", sock, "--log-format", logFmt, "--log-level", logLvl, "\" ' with administrator priviledges")
		case "linux":
			user := super.NewUser("pkexec")
			p.subProc = user.Command(binName, "installer", "-s", sock, "--log-format", logFmt, "--log-level", logLvl)
		case "windows":
			return fmt.Errorf("windows is currently unsupported")
		}
		return nil
	}
}

func getCurBin(s string) (string, error) {
	if filepath.IsAbs(s) || s == "muku" {
		return s, nil
	}

	wd, err := os.Getwd()
	return filepath.Join(wd, s), err
}

func captureSubProcLogs(p *rootCmd, cfg *logConfig) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		out, err := p.subProc.StderrPipe()
		if err != nil {
			return err
		}

		go func() {
			_, err := io.Copy(cfg.out, out)
			if err != nil {
				zap.L().Error("unexpected error when capturing installer process logs", zap.Error(err))
			}
		}()

		return nil
	}
}

func connectToSubProc(p *rootCmd) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) (err error) {
		sockFlag := cmd.Flags().Lookup("socket")
		if sockFlag == nil {
			return fmt.Errorf("expected socket flag")
		}
		sock := sockFlag.Value.String()

		zap.L().Info("starting subprocess...")
		if err = p.subProc.Start(); err != nil {
			return
		}
		zap.L().Info("started subprocess...")

		zap.L().Info("connecting subprocess...")
		p.subProcConn, err = grpc.Dial("unix://"+sock, grpc.WithInsecure(), grpc.WithBlock())
		return
	}
}

type connState uint8

const (
	started connState = iota
	ended
)

func run(rootCtx context.Context, p *rootCmd, logCfg *logConfig, openBrowser bool, domain string) {
	defer p.ls.Close()
	port := p.ls.Addr().(*net.TCPAddr).Port

	appClient := pb.NewAppClient(p.subProcConn)
	ctx := graphql.NewContext(
		rootCtx,
		graphql.WithClient(appClient),
		graphql.WithLogCache(logCfg.cache),
		graphql.WithAppRegistry(p.apps),
	)

	gqlHandler := gws.NewHandler(
		serveGraphQL(ctx, p.schema),
		gws.WithOrigins("localhost:*", "z5labs.gitlab.io"),
		gws.WithMessageType(gws.MessageText),
	)

	mux := http.NewServeMux()
	p.srv.Handler = mux

	connChan := make(chan connState, 2)
	mux.HandleFunc("/graphql", func(w http.ResponseWriter, req *http.Request) {
		connChan <- started
		gqlHandler.ServeHTTP(w, req.WithContext(ctx))
		connChan <- ended
	})

	var r runner
	ready := make(chan struct{}, 1)

	// Start GraphQL service
	r.Go(func(_ context.Context) error {
		zap.L().Info("serving graphql service", zap.Int("port", port))

		ready <- struct{}{}
		close(ready)

		err := p.srv.Serve(p.ls)
		if err != nil && err != http.ErrServerClosed {
			return err
		}
		return ErrShutdown
	})

	// Monitor installer process
	r.Go(func(_ context.Context) error {
		defer zap.L().Info("installer process shutdown")
		return p.subProc.Wait()
	})

	// Shutdown installer process
	r.Go(func(ctx context.Context) error {
		p.subProcConn.WaitForStateChange(ctx, connectivity.Ready)

		connState := p.subProcConn.GetState()
		if connState == connectivity.Shutdown {
			return nil
		}

		zap.L().Info("shutting down installer process")
		_, err := appClient.Shutdown(context.TODO(), &empty.Empty{})
		if err == nil {
			return p.subProcConn.Close()
		}

		stat := status.Convert(err)
		if stat.Code() == codes.Unavailable {
			return nil
		}
		return stat.Err()
	})

	// Shut process down when there are no more connections
	r.Go(func(ctx context.Context) error {
		<-ready
		return watchConns(ctx, p.srv, connChan)
	})

	// Open browser to install route
	r.Go(func(_ context.Context) error {
		if !openBrowser {
			return nil
		}

		url := fmt.Sprintf("%s/?port=%d", domain, port)
		zap.L().Info("opening browser to URL", zap.String("domain", url))
		return browser.Open(url)
	})

	err := r.Wait(rootCtx)
	if err != nil {
		zap.L().Error("encountered unexpected error", zap.Error(err))
	}
	// TODO: Tell subprocess to shutdown
}

func serveGraphQL(ctx context.Context, schema gql.Schema) gws.HandlerFunc {
	return func(s *gws.Stream, req *gws.Request) error {
		zap.L().Debug("executing query", zap.String("query", req.Query))

		params := gql.Params{
			Schema:         schema,
			RequestString:  req.Query,
			VariableValues: req.Variables,
			OperationName:  req.OperationName,
			Context:        graphql.NewContext(ctx, graphql.WithStream(s)),
		}

		res := gql.Do(params)
		b, err := json.Marshal(res.Data)
		if err != nil {
			zap.L().Error("failed to marshal query results", zap.Error(err))
			return err
		}

		resp := &gws.Response{Data: b}
		for _, rerr := range res.Errors {
			b, err = json.Marshal(rerr)
			if err != nil {
				zap.L().Error("failed to marshal resolver error", zap.Error(err))
				continue
			}
			resp.Errors = append(resp.Errors, json.RawMessage(b))
		}

		// This forces the streams to be solely managed by
		// gws.Handler a.k.a client must send stop message
		// for streams to properly clean up.
		//
		// Should probably close queries and let gws.Handler
		// manage subscriptions
		//
		return s.Send(ctx, resp)
	}
}

func watchConns(ctx context.Context, srv *http.Server, connChan <-chan connState) error {
	conns := 0

	for {
		select {
		case <-ctx.Done():
			return srv.Shutdown(context.TODO())
		case s := <-connChan:
			if s == started {
				conns++
				break
			}
			conns--
		}

		if conns > 0 {
			continue
		}

		return srv.Shutdown(context.TODO())
	}
}
