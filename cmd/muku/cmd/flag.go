// flag.go provides custom flags for the cli

package cmd

import (
	"os"
	"path/filepath"

	"go.uber.org/zap/zapcore"
)

type socketAddrFlag string

func (f *socketAddrFlag) String() string { return string(*f) }

func (f *socketAddrFlag) Type() string { return "socket address" }

func (f *socketAddrFlag) Set(val string) (err error) {
	if filepath.IsAbs(val) {
		*f = socketAddrFlag(val)
		return
	}

	tmpDir := os.TempDir()
	if tmpDir == "" {
		tmpDir, err = os.Getwd()
	}

	*f = socketAddrFlag(filepath.Join(tmpDir, val))
	return
}

type logLevelFlag struct {
	zapcore.Level
}

func (f *logLevelFlag) Type() string { return "level" }
