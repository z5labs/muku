package cmd

import (
	"muku/app"

	"github.com/spf13/cobra"
)

type cmder interface {
	getCommand() *cobra.Command
}

type cli struct {
	cmds []cmder
	apps *app.Registry
}

func (c *cli) addCommands(cmds ...cmder) *cli {
	c.cmds = append(c.cmds, cmds...)
	return c
}

func (c *cli) build() *cobra.Command {
	cmd := c.newRootCmd(c.apps).getCommand()

	for _, cm := range c.cmds {
		cmd.AddCommand(cm.getCommand())
	}

	return cmd
}

// NewCLI initializes the tcs-installer CLI with the given apps.
func NewCLI(apps *app.Registry) *cobra.Command {
	c := &cli{apps: apps}

	return c.addCommands(
		c.newInstallerCmd(apps),
	).build()
}

func withPreRuns(onError func(error), fs ...func(*cobra.Command, []string) error) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		for _, f := range fs {
			if err := f(cmd, args); err != nil {
				onError(err)
				return err
			}
		}

		return nil
	}
}
