package cmd

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime/pprof"
	"runtime/trace"

	"github.com/spf13/cobra"
)

// profs represents the output writers for profiling
type profs struct {
	traceOut io.Writer
	cpuOut   io.Writer
}

func startTracing(p *profs) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		traceDir, err := cmd.Flags().GetString("trace")
		if err != nil || traceDir == "" {
			return err
		}

		err = os.MkdirAll(traceDir, os.ModeDir)
		if err != nil {
			return err
		}

		traceName := filepath.Join(traceDir, fmt.Sprintf("muku_%s.trace", cmd.Name()))
		p.traceOut, err = os.Create(traceName)
		if err != nil {
			return err
		}

		return trace.Start(p.traceOut)
	}
}

func stopTracing(p *profs) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		traceDir, err := cmd.Flags().GetString("trace")
		if err != nil || traceDir == "" {
			return err
		}
		trace.Stop()

		wc, ok := p.traceOut.(io.WriteCloser)
		if !ok {
			return nil
		}
		return wc.Close()
	}
}

func startPprofProfiles(p *profs) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		cpuDir, err := cmd.Flags().GetString("profile-cpu")
		if err != nil || cpuDir == "" {
			return err
		}

		err = os.MkdirAll(cpuDir, os.ModeDir)
		if err != nil {
			return err
		}

		cpuName := filepath.Join(cpuDir, fmt.Sprintf("muku_%s.cpu", cmd.Name()))
		p.cpuOut, err = os.Create(cpuName)
		if err != nil {
			return err
		}

		return pprof.StartCPUProfile(p.cpuOut)
	}
}

func stopPprofProfiles(p *profs) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		cpuDir, cerr := cmd.Flags().GetString("profile-cpu")
		memDir, merr := cmd.Flags().GetString("profile-mem")

		if cerr != nil || cpuDir == "" && memDir == "" {
			return cerr
		}
		if merr != nil || cpuDir == "" && memDir == "" {
			return merr
		}

		pprof.StopCPUProfile()

		wc, ok := p.cpuOut.(io.WriteCloser)
		if !ok {
			return nil
		}
		return wc.Close()
	}
}

func writeMemProfile(cmd *cobra.Command, args []string) error {
	memDir, err := cmd.Flags().GetString("profile-mem")
	if err != nil || memDir == "" {
		return err
	}

	err = os.MkdirAll(memDir, os.ModeDir)
	if err != nil {
		return err
	}

	memName := filepath.Join(memDir, fmt.Sprintf("muku_%s.mem", cmd.Name()))
	f, err := os.Create(memName)
	if err != nil {
		return err
	}
	defer f.Close()

	return pprof.WriteHeapProfile(f)
}
