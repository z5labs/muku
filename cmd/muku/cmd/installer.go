package cmd

import (
	"context"
	"net"
	"os"
	"sync"

	"muku/app"
	pb "muku/cmd/proto"

	empty "github.com/golang/protobuf/ptypes/empty"
	"github.com/spf13/cobra"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

type installCmd struct {
	*cobra.Command
	apps *app.Registry
	ls   net.Listener
}

func (cmd *installCmd) getCommand() *cobra.Command { return cmd.Command }

func (c *cli) newInstallerCmd(apps *app.Registry) *installCmd {
	cmd := &installCmd{
		Command: &cobra.Command{
			Use:   "installer",
			Short: "installer handles installing applications.",
		},
		apps: apps,
	}

	cmd.Flags().StringP("sock", "s", "", "Specify the UDS for IPC")

	cmd.PreRunE = startInstallListener(cmd)
	cmd.Run = install(cmd)

	return cmd
}

func startInstallListener(c *installCmd) func(*cobra.Command, []string) error {
	return func(cmd *cobra.Command, args []string) error {
		sock, err := cmd.Flags().GetString("sock")
		if err != nil {
			return err
		}

		c.ls, err = net.ListenUnix("unix", &net.UnixAddr{Name: sock, Net: "unix"})
		if err != nil {
			return err
		}

		err = os.Chmod(sock, 0777)
		if err != nil {
			c.ls.Close()
			return err
		}

		return nil
	}
}

func install(c *installCmd) func(*cobra.Command, []string) {
	return func(cmd *cobra.Command, args []string) {
		defer c.ls.Close()

		ctx, cancel := context.WithCancel(cmd.Context())
		defer cancel()

		srv := grpc.NewServer()

		appService := &server{
			apps:     c.apps,
			shutdown: func() { cancel() },
		}

		pb.RegisterAppServer(srv, appService)

		errChan := make(chan error, 1)
		go func() {
			defer close(errChan)
			zap.L().Info("grpc service started")

			err := srv.Serve(c.ls)
			errChan <- err
		}()

		select {
		case <-ctx.Done():
			zap.L().Info("shutting down")
			srv.GracefulStop()
			<-errChan
			return
		case err := <-errChan:
			if err != nil {
				zap.L().Error("unexpected error from grpc server", zap.Error(err))
			}
			return
		}
	}
}

type server struct {
	apps     *app.Registry
	shutOnce sync.Once
	shutdown func()
}

func (s *server) Install(req *pb.AppInfo, stream pb.App_InstallServer) error {
	resp := &pb.InstallStatus{}

	stats := s.apps.Install(stream.Context(), req.Id, req.Path, app.Version(req.Version))
	for stat := range stats {
		switch {
		case stat.Failed != nil:
			resp.Status = &pb.InstallStatus_Failed{
				Failed: stat.Failed.Error(),
			}
			break
		case stat.Progress != nil:
			resp.Status = &pb.InstallStatus_Progress{
				Progress: stat.Progress.String(),
			}
			break
		default:
			resp.Status = &pb.InstallStatus_Installed{
				Installed: &pb.Installed{
					Path: stat.Installed.Path,
					Time: uint64(stat.Installed.Time),
				},
			}
		}

		if err := stream.Send(resp); err != nil {
			return err
		}
	}

	return nil
}

func (s *server) Shutdown(_ context.Context, _ *empty.Empty) (*empty.Empty, error) {
	// Dedup repeated shutdown calls
	s.shutOnce.Do(func() {
		go s.shutdown()
	})
	return &empty.Empty{}, nil
}
