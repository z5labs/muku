// Package app provides a consistent API for installing apps.
package app

import (
	"context"
	"fmt"
	"time"
)

// Version is possible app version.
type Version string

// Progress represents the state an installation is in.
type Progress interface {
	String() string
}

// ProgressStr implements the Progress interface for strings.
type ProgressStr string

func (s ProgressStr) String() string { return string(s) }

// Latest specifies the installer to install the latest version of an app.
const Latest Version = "latest"

const (
	Downloading ProgressStr = "downloading"
	Downloaded  ProgressStr = "downloaded"
	Installing  ProgressStr = "installing"
	Building    ProgressStr = "building"
)

// Download represents the progress of a download.
type Download struct {
	Percent float64
}

func (p Download) String() string {
	return fmt.Sprintf("Downloaded: %5.1f%%", p.Percent)
}

// Installed represents a successful installation.
type Installed struct {
	// Path is the location of the installed app.
	Path string

	// Time is the amount of time it took to install.
	Time time.Duration
}

// Status represents the current install status of an app.
type Status struct {
	// Failed represents a failed installation.
	Failed error

	// Installed represents a successful installation.
	Installed *Installed

	// Progress represents an in progress installation.
	Progress Progress
}

// Installer represents an app installer.
type Installer interface {
	// Install installs a given version of an app.
	Install(ctx context.Context, path string, version Version) <-chan Status
}

// Info
type Info struct {
	Id          string
	Description string
	Installed   bool
	Path        string
	Version     string
	Thumbnail   string
}

// Manager consolidates all app related functionality
type Manager interface {
	Installer

	// Info
	Info() Info
}

// Registry holds a collection of apps and their installers.
type Registry struct {
	apps map[string]Manager
}

func NewRegistry() *Registry {
	return &Registry{
		apps: make(map[string]Manager),
	}
}

// Uninstalled signifies that the registered app is uninstalled.
func Uninstalled() string { return "" }

// Register registers an app installer with the app package.
func (r *Registry) Register(m Manager) {
	id := m.Info().Id
	if _, ok := r.apps[id]; ok {
		panic(fmt.Sprintf("app is already registered with id: %s", id))
	}
	r.apps[id] = m
}

// Install installs an app with a given version.
func (r *Registry) Install(ctx context.Context, id, path string, version Version) <-chan Status {
	return r.apps[id].Install(ctx, path, version)
}

// AllInfo returns all the info for each registered app.
func (r *Registry) AllInfo() []Info {
	infos := make([]Info, 0, len(r.apps))
	for _, m := range r.apps {
		infos = append(infos, m.Info())
	}
	return infos
}
