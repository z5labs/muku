package app

import (
	"context"
	"os"
	"path/filepath"
	"reflect"
	"testing"
)

func TestGolang(t *testing.T) {
	wd, err := os.Getwd()
	if err != nil {
		t.Error(err)
		return
	}
	rootPath := filepath.Join(wd, "go")

	defer os.RemoveAll(rootPath)

	g := new(Golang)

	stats := g.Install(context.Background(), rootPath, Latest)

	expect(t, Status{Progress: Installing}, <-stats)
	for stat := range stats {
		if stat.Progress == nil {
			expect(t, Status{Installed: &Installed{Path: rootPath}}, stat)
			continue
		}

		_, ok := stat.Progress.(Download)
		if !ok {
			t.Fatalf("expected download progress but got: %s", stat.Progress)
		}
	}
}

func expect(t *testing.T, ex, got Status) {
	if ex.Installed != nil {
		if ex.Installed.Path != got.Installed.Path {
			t.Fatalf("expected: %v\nreceived: %v", ex, got)
		}
		return
	}

	if !reflect.DeepEqual(ex, got) {
		t.Fatalf("expected: %v\nreceived: %v", ex, got)
	}
}
