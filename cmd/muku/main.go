package main

import (
	"context"
	"os"
	"os/signal"

	"muku/app"
	"muku/cmd"

	"go.uber.org/zap"
)

func main() {
	defer zap.L().Sync()

	// Init app registry
	apps := app.NewRegistry()
	apps.Register(new(app.Golang))

	sigCtx, sigCancel := handleSigs(context.Background())
	cmdCtx, cmdCancel := context.WithCancel(sigCtx)

	c := cmd.NewCLI(apps)

	errChan := make(chan error, 1)
	go func() {
		defer close(errChan)
		defer cmdCancel()

		err := c.ExecuteContext(cmdCtx)
		errChan <- err
	}()

	select {
	case <-sigCtx.Done():
		cmdCancel()
	case <-cmdCtx.Done():
		sigCancel()
		<-sigCtx.Done()
	}

	err := <-errChan
	if err != nil {
		zap.L().Fatal("unexpected error", zap.Error(err))
	}
}

func handleSigs(p context.Context) (ctx context.Context, cancel context.CancelFunc) {
	ctx, cancel = context.WithCancel(p)

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)

	go func() {
		defer cancel()
		defer close(sigChan)
		defer signal.Stop(sigChan)

		select {
		case <-ctx.Done():
			return
		case <-sigChan:
			return
		}
	}()

	return
}
