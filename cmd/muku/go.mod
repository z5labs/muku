module muku

go 1.13

require (
	github.com/golang/protobuf v1.3.5
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/graphql-go/graphql v0.7.9
	github.com/spf13/cobra v0.0.6
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/zaba505/gws v0.5.0
	go.uber.org/zap v1.15.0
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	google.golang.org/genproto v0.0.0-20200324203455-a04cca1dde73 // indirect
	google.golang.org/grpc v1.28.0
)
